<?php

use Tester\Tester;

include __DIR__ . '/vendor/autoload.php';

Tester::start(__DIR__ . '/source/', new \LuckyTicket\LuckyTicket());
