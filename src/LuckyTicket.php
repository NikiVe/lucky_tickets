<?php


namespace LuckyTicket;


use Tester\TestFunc;

class LuckyTicket implements TestFunc
{
    protected $n;

    protected $totalSum = [];

    public function run(string $value) : string {
        $this->n = (int)$value;
        $luckyTickets = 0;
        $this->init();

        for ($i = 2; $i <= $this->n; $i++) {
            $maxSum = $i * 9;
            $maxTotalSum = $maxSum - 9;

            $digits = $this->totalSum;

            $this->totalSum = $this->clearUp($this->totalSum, $maxSum);

            for ($s = 0; $s <= 9; $s++) {
                for ($j = 0; $j <= $maxTotalSum; $j++) {
                    $this->totalSum[$s + $j] += $digits[$j];
                }
            }
        }

        for ($i = 0; $i <= $this->n * 9; $i++) {
            $luckyTickets += $this->totalSum[$i] ** 2;
        }
        return $luckyTickets;
    }

    /**
     * Инициализирует массив единицами.
     */
    protected function init() {
        for ($i = 0; $i <= 9; $i++) {
            $this->totalSum[$i] = 1;
        }
    }

    public function clearUp(array $array, $position) {
        for ($i = 0; $i <= $position; $i++) {
            $array[$i] = 0;
        }
        return $array;
    }
}